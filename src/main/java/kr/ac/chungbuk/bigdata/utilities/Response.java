package kr.ac.chungbuk.bigdata.utilities;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by PENHCHET on 11/21/2016.
 */
@Data
public class Response<T> implements Serializable {
    private String code;
    private String message;
    private T data;

    public Response(T data, ResponseCode code){
        this.data = data;
        this.code = code.toString();
        this.message = code.getMessage();
    }

}
