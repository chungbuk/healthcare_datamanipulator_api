package kr.ac.chungbuk.bigdata.utilities;

/**
 * Created by PENHCHET on 11/21/2016.
 */
public enum ResponseCode {

    SUCCESS("0000", "Successful"),
    FAILURE("9999", "Failure");

    private final String value;
    private final String reasonPhrase;

    private ResponseCode(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    public String toString() {
        return this.value;
    }

    public String getMessage() { return this.reasonPhrase; }
}
