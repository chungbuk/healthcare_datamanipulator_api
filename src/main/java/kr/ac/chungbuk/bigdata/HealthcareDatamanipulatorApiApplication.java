package kr.ac.chungbuk.bigdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HealthcareDatamanipulatorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HealthcareDatamanipulatorApiApplication.class, args);
	}
}
