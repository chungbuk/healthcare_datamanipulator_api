package kr.ac.chungbuk.bigdata.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDataset implements Serializable {
	private int ud_idx;
	private String ud_name;
	private String ud_description;
	private String ud_regdate;
	private String ud_terms_db_op1;
	private String ud_terms_db_op2;
	private String ud_terms_db_op3;
	private String ud_terms_disease_code;
	private String ud_terms_disease_target;
	private String ud_terms_medicine_code;
	private String ud_terms_materials_code;
	private String ud_terms_treatment_code;
//	private String ud_terms_disease_keyword;
//	private String ud_terms_medicine_keyword;
//	private String ud_terms_materials_keyword;
//	private String ud_terms_treatment_keyword;
	private String ud_status;
	private String ud_start_time;
	private String ud_end_time;

}
