package kr.ac.chungbuk.bigdata.models;

public interface Code {
	String STARTED = "started";
	String FAILED = "failed";
	String COMPLETED = "completed";
	String HIRA = "심사평가원";
	String NHIC = "건강보험공단";
}
