package kr.ac.chungbuk.bigdata.models;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class MemberToUserDataset implements Serializable{
	private int mtud_idx;
	private int mem_idx;
	private int ud_idx;
	private String mtud_tb_name;
	private String mtud_type;
	private String mtud_row_count;
	private String mtud_file_size;
	private Timestamp mtud_regdate;
}
