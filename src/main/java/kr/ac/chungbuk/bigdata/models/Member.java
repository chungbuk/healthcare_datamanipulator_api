package kr.ac.chungbuk.bigdata.models;

import lombok.Data;

@Data
public class Member {
    private int mem_idx;
    private String mem_email;
    private String mem_pw;
    private String mem_regdate;
}
