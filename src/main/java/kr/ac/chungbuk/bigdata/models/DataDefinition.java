package kr.ac.chungbuk.bigdata.models;

import lombok.Data;

import java.io.Serializable;

@SuppressWarnings("serial")
@Data
public class DataDefinition implements Serializable {
	private UserDataset userdataset;
	private MemberToUserDataset membertouserdataset;
	private String downloadURL;
}
