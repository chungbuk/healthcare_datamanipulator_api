package kr.ac.chungbuk.bigdata.repositories.mappers;

import kr.ac.chungbuk.bigdata.models.MemberToUserDataset;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MTUDRowMapper implements RowMapper<MemberToUserDataset> {

	@Override
	public MemberToUserDataset mapRow(ResultSet rs, int rowNum) throws SQLException {
		MemberToUserDataset mtud = new MemberToUserDataset();
		mtud.setMtud_idx(rs.getInt("mtud_idx"));
		mtud.setMem_idx(rs.getInt("mem_idx"));
		mtud.setUd_idx(rs.getInt("ud_idx"));
		mtud.setMtud_tb_name(rs.getString("mtud_tb_name"));
		mtud.setMtud_type(rs.getString("mtud_type"));
		mtud.setMtud_row_count(rs.getString("mtud_row_count"));
		mtud.setMtud_file_size(rs.getString("mtud_file_size"));
		mtud.setMtud_regdate(rs.getTimestamp("mtud_regdate"));
		return mtud;
	}

}
