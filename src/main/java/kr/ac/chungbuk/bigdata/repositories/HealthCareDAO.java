package kr.ac.chungbuk.bigdata.repositories;

import kr.ac.chungbuk.bigdata.models.DataDefinition;
import kr.ac.chungbuk.bigdata.models.Member;
import kr.ac.chungbuk.bigdata.models.MemberToUserDataset;
import kr.ac.chungbuk.bigdata.models.UserDataset;
import org.springframework.dao.DataAccessException;

import java.util.List;

@SuppressWarnings("deprecation")
public interface HealthCareDAO {
	/**
	 * FIND 사용자데이터셋
	 * 
	 * @return
	 */
	public List<UserDataset> findAll();

	/**
	 * VALIDATION 사용자데이터셋
	 * 
	 * @param ud
	 * @param mtud
	 * @return
	 */
	public boolean checkUserDataset(UserDataset ud, MemberToUserDataset mtud);

	/**
	 * UPDATE STATUS('started','completed','failed') of 사용자데이터셋
	 * 
	 * @param ud
	 * @param mtud
	 * @param flag
	 */
	public void updateUserDataset(UserDataset ud, MemberToUserDataset mtud, String flag);

	public List<Member> findAllMember();

	/**
	 * TEST ONLY
	 * 
	 */
	public void testHive();
	public void updateStatistics4NHICTest(UserDataset ud);
	public void updateStatistics4HiraTest(UserDataset ud);
	public void createTable(String tableName, String query) throws DataAccessException;
	public void updateTableSize(DataDefinition dataDef);

	public void testCreateTable();
	public void testTableSize();
	public void updateStatistics(DataDefinition dataDef);

	public void updateTableInformation(DataDefinition dataDef);
}
