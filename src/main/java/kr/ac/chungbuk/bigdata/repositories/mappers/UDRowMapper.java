package kr.ac.chungbuk.bigdata.repositories.mappers;

import kr.ac.chungbuk.bigdata.models.UserDataset;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UDRowMapper implements RowMapper<UserDataset> {

	@Override
	public UserDataset mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserDataset userDataset = new UserDataset();
		userDataset.setUd_idx(rs.getInt("ud_idx"));
		userDataset.setUd_name(rs.getString("ud_name"));
		userDataset.setUd_description(rs.getString("ud_description"));
		userDataset.setUd_regdate(rs.getString("ud_regdate"));
		userDataset.setUd_terms_db_op1(rs.getString("ud_terms_db_op1"));
		userDataset.setUd_terms_db_op2(rs.getString("ud_terms_db_op2"));
		userDataset.setUd_terms_db_op3(rs.getString("ud_terms_db_op3"));
		userDataset.setUd_terms_disease_code(rs.getString("ud_terms_disease_code"));
		userDataset.setUd_terms_disease_target(rs.getString("ud_terms_disease_target"));
		userDataset.setUd_terms_medicine_code(rs.getString("ud_terms_medicine_code"));
		userDataset.setUd_terms_materials_code(rs.getString("ud_terms_materials_code"));
		userDataset.setUd_terms_treatment_code(rs.getString("ud_terms_treatment_code"));
		userDataset.setUd_status(rs.getString("ud_status"));
		userDataset.setUd_start_time(rs.getString("ud_start_time"));
		userDataset.setUd_end_time(rs.getString("ud_end_time"));
		return userDataset;

	}

}
