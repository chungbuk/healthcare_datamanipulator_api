package kr.ac.chungbuk.bigdata.repositories;

import kr.ac.chungbuk.bigdata.models.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("deprecation")
@Repository
public class HealthCareDAOImpl implements HealthCareDAO {
    private final String USER_DATASET_TABLE_NAME = "_user_dataset";
    private final String MEMBER_TO_USER_DATASET_TABLE_NAME = "_member_to_user_dataset";
    private final String USER_DATASET_STATISTICS_TABLE_NAME = "_user_dataset_statistics";
    @SuppressWarnings("unused")
    private final String MEMBER_TABLE_NAME = "_member";
    Logger logger = Logger.getLogger(getClass());

//    @SuppressWarnings("unused")
//    private DataSource dataSource;
//    @SuppressWarnings("unused")
//    private DataSource hiveDataSource;

    @Autowired
    @Qualifier("sqlServerJdbcTemplate")
    private JdbcTemplate sqlServerJdbcTemplate;

    @Autowired
    @Qualifier("thriftServerJdbcTemplate")
    private JdbcTemplate hiveJdbcTemplate;

//    public void setDataSource(DataSource dataSource) {
//        this.dataSource = dataSource;
//        this.sqlServerJdbcTemplate = new JdbcTemplate(dataSource);
//    }
//
//    public void setHiveDataSource(DataSource hiveDataSource) {
//        this.hiveDataSource = hiveDataSource;
//        this.hiveJdbcTemplate = new JdbcTemplate(hiveDataSource);
//    }

    @Override
    public List<UserDataset> findAll() {

        final String sql = "SELECT * FROM " + USER_DATASET_TABLE_NAME;

        List<UserDataset> list = new ArrayList<UserDataset>();
        List<Map<String, Object>> rows = sqlServerJdbcTemplate.queryForList(sql);

        for (Map<String, Object> row : rows) {
            UserDataset data = new UserDataset();
            data.setUd_idx(Integer.parseInt(String.valueOf(row.get("ud_idx"))));
            data.setUd_name(String.valueOf(row.get("ud_name")));
            data.setUd_description(String.valueOf(row.get("ud_description")));
            data.setUd_regdate(String.valueOf(row.get("ud_regdate")));
            data.setUd_terms_db_op1(String.valueOf(row.get("ud_terms_db_op1")));
            data.setUd_terms_db_op2(String.valueOf(row.get("ud_terms_db_op2")));
            data.setUd_terms_db_op3(String.valueOf(row.get("ud_terms_db_op3")));
            data.setUd_terms_disease_code(String.valueOf(row.get("ud_terms_disease_code")));
            data.setUd_terms_disease_target(String.valueOf(row.get("ud_terms_disease_target")));
            data.setUd_terms_medicine_code(String.valueOf(row.get("ud_terms_medicine_code")));
            data.setUd_terms_materials_code(String.valueOf(row.get("ud_terms_materials_code")));
            data.setUd_terms_treatment_code(String.valueOf(row.get("ud_terms_treatment_code")));
            data.setUd_status(String.valueOf(row.get("ud_status")));
            data.setUd_start_time(String.valueOf(row.get("ud_start_time")));
            data.setUd_end_time(String.valueOf(row.get("ud_end_time")));

            list.add(data);
        }
        // data.setUd_terms_disease_keyword(String.valueOf(row.get("ud_terms_disease_keyword")));
        // data.setUd_terms_medicine_keyword(String.valueOf(row.get("ud_terms_medicine_keyword")));
        // data.setUd_terms_materials_keyword(String.valueOf(row.get("ud_terms_materials_keyword")));

        return list;
    }

    @Override
    public List<Member> findAllMember() {
        final String sql = "SELECT mem_idx,mem_email,mem_pw,mem_regdate FROM _member";

        List<Member> list = new ArrayList<Member>();
        List<Map<String, Object>> rows = sqlServerJdbcTemplate.queryForList(sql);

        for (Map<String, Object> row : rows) {
            Member data = new Member();
            data.setMem_idx(Integer.parseInt(String.valueOf(row.get("mem_idx"))));
            data.setMem_email(String.valueOf(row.get("mem_email")));
            data.setMem_pw(String.valueOf(row.get("mem_pw")));
            data.setMem_regdate(String.valueOf(row.get("mem_regdate")));
            list.add(data);
        }

        return list;
    }

    @Override
    public boolean checkUserDataset(final UserDataset ud, final MemberToUserDataset mtud) {
        boolean exist = false;

        final String sql1 = "SELECT * FROM " + USER_DATASET_TABLE_NAME + " WHERE ud_idx = ?";
        UserDataset rsvUD = sqlServerJdbcTemplate.queryForObject(sql1, new Object[]{ud.getUd_idx()},
                new BeanPropertyRowMapper<UserDataset>(UserDataset.class));
        if (rsvUD.getUd_idx() == ud.getUd_idx() && rsvUD.getUd_name().equals(rsvUD.getUd_name())) {
            exist = true;
            logger.info("USER_DATASET_IDX:" + ud.getUd_idx() + " is valid");

            final String sql2 = "SELECT * FROM " + MEMBER_TO_USER_DATASET_TABLE_NAME
                    + " WHERE mtud_idx = ? AND mem_idx = ? AND ud_idx = ? ";
            MemberToUserDataset rsvMTUD = sqlServerJdbcTemplate.queryForObject(sql2,
                    new Object[]{mtud.getMtud_idx(), mtud.getMem_idx(), mtud.getUd_idx()},
                    new BeanPropertyRowMapper<MemberToUserDataset>(MemberToUserDataset.class));

            if (rsvMTUD == null) {
                exist = false;
                logger.info("mtud_idx:" + mtud.getUd_idx() + "mem_idx:" + mtud.getMem_idx() + "ud_idx:"
                        + mtud.getUd_idx() + " is INVALID");
            } else {
                logger.debug("mtud_idx:" + mtud.getUd_idx() + "mem_idx:" + mtud.getMem_idx() + "ud_idx:"
                        + mtud.getUd_idx() + " is valid");
            }
            logger.debug(rsvUD.toString() + "\n" + rsvMTUD.toString());
        }

        return exist;
        // UserDataset rsvUD = jdbcTemplate.queryForObject(sql1, new Object[] {
        // ud.getUd_idx() }, new UDRowMapper());
        // MemberToUserDataset rsvMTUD = jdbcTemplate.queryForObject(sql2,
        // new Object[] { mtud.getMtud_idx(), mtud.getMem_idx(),
        // mtud.getUd_idx() }, new MTUDRowMapper());
    }

    @Override
    public void updateUserDataset(UserDataset ud, MemberToUserDataset mtud, String flag) {

        String sql = "";
        switch (flag) {
            case Code.STARTED:
                sql = "UPDATE " + USER_DATASET_TABLE_NAME
                        + " SET ud_regdate=?, ud_status=?, ud_start_time=? WHERE ud_idx=? AND ud_name=?";
                break;

            case Code.FAILED:
                sql = "UPDATE " + USER_DATASET_TABLE_NAME + " SET ud_regdate=?, ud_status=?, ud_end_time=? WHERE ud_idx=? AND ud_name=?";

                break;

            case Code.COMPLETED:
                sql = "UPDATE " + USER_DATASET_TABLE_NAME + " SET ud_regdate=?, ud_status=?, ud_end_time=? WHERE ud_idx=? AND ud_name=?";
                break;

            default:
                break;
        }

        int affectedRows = sqlServerJdbcTemplate.update(sql,
                new Object[]{new Timestamp(System.currentTimeMillis()), flag, new Timestamp(System.currentTimeMillis()), ud.getUd_idx(), ud.getUd_name()});
        logger.debug("affectedRows : " + affectedRows);
        int user_idx = mtud.getMem_idx();
        int user_dataset_idx = ud.getUd_idx();
        String userTableName = "user_datasets.ud_" + user_idx + "_" + user_dataset_idx;
        String mtudTableInformationUpdateQuery = "UPDATE _member_to_user_dataset SET mtud_tb_name=?, mtud_regdate=? WHERE mtud_idx=?";
        sqlServerJdbcTemplate.update(mtudTableInformationUpdateQuery, new Object[]{userTableName, new Timestamp(System.currentTimeMillis()), mtud.getMtud_idx()});

    }

    @Override
    public void updateStatistics(DataDefinition dataDef) {
        int user_idx = dataDef.getMembertouserdataset().getMem_idx();
        int user_dataset_idx = dataDef.getUserdataset().getUd_idx();
        String userTableName = "user_datasets.ud_" + user_idx + "_" + user_dataset_idx;
        String sql = "";
        switch (dataDef.getUserdataset().getUd_terms_db_op1()) {
            case Code.HIRA:
                sql = "select GROUPING_ID() as group_ID,t20_sex_tp_cd GENDER, t20_agg AGE_GROUP, sa_ykiho_sido SIDO,year(t20_recu_fr_dd) year, month(t20_recu_fr_dd) month, count(T20_KEY_SEQ) n_statement, count(distinct t20_person_id) as n_patient, sum(t20_RVD_RPE_TAMT) as n_expense "
                        + "from " + userTableName + " "
                        + "GROUP BY year(t20_recu_fr_dd), month(t20_recu_fr_dd), t20_sex_tp_cd, t20_agg, sa_ykiho_sido WITH CUBE";
                break;
            case Code.NHIC:
                sql = "SELECT GROUPING_ID() as group_ID,JK_SEX GENDER, JK_AGE_GROUP AGE_GROUP,"
                        + " JK_SIDO SIDO,year(t30_recu_fr_dt) year, month(t30_recu_fr_dt) month,"
                        + " count(T20_KEY_SEQ) n_statement, count(distinct T20_PERSON_ID) as n_patient, "
                        + "sum(T20_DMD_TRAMT) as n_expense " + "from " + userTableName + " "
                        + "GROUP BY year(t30_recu_fr_dt), month(t30_recu_fr_dt), JK_SEX, JK_AGE_GROUP, JK_SIDO WITH CUBE";
                break;

            default:
                break;
        }

        List<Map<String, Object>> rows = hiveJdbcTemplate.queryForList(sql);
        String insertQuery = "INSERT INTO " + USER_DATASET_STATISTICS_TABLE_NAME
                + "(ud_idx,group_id,gender,age_group,sido,year,month,n_statement,n_patient,n_expense) VALUES ( ?,?,?,?,?,?,?,?,?,?)";
        for (Map<String, Object> row : rows) {
            int groupID = Integer.parseInt((row.get("group_ID") == null ? "0" : String.valueOf(row.get("group_ID"))));
            if (groupID > 0)//0,1,2,3,4,5,6,7
                continue;
            int gender = Integer.parseInt((row.get("GENDER") == null ? "0" : String.valueOf(row.get("GENDER"))));
            int AGE_GROUP = Integer.parseInt((row.get("AGE_GROUP") == null ? "0" : String.valueOf(row.get("AGE_GROUP"))));
            int SIDO = Integer.parseInt((row.get("SIDO") == null ? "0" : String.valueOf(row.get("SIDO"))));
            int year = Integer.parseInt((row.get("year") == null ? "0" : String.valueOf(row.get("year"))));
            int month = Integer.parseInt((row.get("month") == null ? "0" : String.valueOf(row.get("month"))));
            long n_statement = Long.parseLong((row.get("n_statement") == null ? "0" : String.valueOf(row.get("n_statement"))));
            long n_patient = Long.parseLong((row.get("n_patient") == null ? "0" : String.valueOf(row.get("n_patient"))));
            double n_expense = Double.parseDouble((row.get("n_expense") == null ? "0.0" : String.valueOf(row.get("n_expense"))));

            int affectedRows = sqlServerJdbcTemplate.update(insertQuery, new Object[]{user_dataset_idx, groupID,
                    gender, AGE_GROUP, SIDO, year, month, n_statement, n_patient, n_expense});
            // System.out.println(String.valueOf(row.toString()));
            logger.debug("inserted rows : " + affectedRows + ", " + row.toString());
            // String.valueOf(row.get("group_id"));
        }

    }

    @Override
    public void updateTableInformation(DataDefinition dataDef) {
        int user_idx = dataDef.getMembertouserdataset().getMem_idx();
        int user_dataset_idx = dataDef.getUserdataset().getUd_idx();
        String userTableName = "user_datasets.ud_" + user_idx + "_" + user_dataset_idx;
        String sql = "SELECT COUNT(*) count FROM " + userTableName;
        Map<String, Object> queryForMap = hiveJdbcTemplate.queryForMap(sql);
        long count = Long.parseLong(String.valueOf(queryForMap.get("count")));

        sqlServerJdbcTemplate.update("UPDATE _user_dataset SET ud_row_count=?, ud_result_file_size=? WHERE ud_idx = ?", new Object[]{count, count * 1000, user_dataset_idx});
    }
    // public void insert(Customer customer){
    //
    // String sql = "INSERT INTO CUSTOMER " +
    // "(CUST_ID, NAME, AGE) VALUES (?, ?, ?)";
    //
    // getJdbcTemplate().update(sql, new Object[] { customer.getCustId(),
    // customer.getName(),customer.getAge()
    // });

    @Override
    public void updateStatistics4HiraTest(UserDataset ud) {
        // select count(t20_KEY_SEQ) as 명세서수, count(distinct t20_person_id) as
        // 환자수, sum(t20_RVD_RPE_TAMT) as 진료비 from NPS_T20 t20 ,Sanatorium group
        // by t20_sex_tp_cd,sa_ykiho_sido
        String tableName = "user_datasets.ud_1_27";
        String sql = "select GROUPING_ID() as group_ID,t20_sex_tp_cd GENDER, t20_agg AGE_GROUP, sa_ykiho_sido SIDO,year(t20_recu_fr_dd) year, month(t20_recu_fr_dd) month, count(T20_KEY_SEQ) n_statement, count(distinct t20_person_id) as n_patient, sum(t20_RVD_RPE_TAMT) as n_expense "
                + "from " + tableName + " "
                + "GROUP BY year(t20_recu_fr_dd), month(t20_recu_fr_dd), t20_sex_tp_cd, t20_agg, sa_ykiho_sido WITH CUBE";
        List<Map<String, Object>> rows1 = hiveJdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows1) {
            System.out.println(String.valueOf(row.toString()));
            String.valueOf(row.get("group_id"));
        }
    }

    @Override
    public void testHive() {

        String tableName = "hira_r2.extraction_nps";
        String sql = "select *  from  " + tableName + " limit 5";
        List<Map<String, Object>> rows1 = hiveJdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows1) {
            System.out.println(String.valueOf(row.toString()));
        }
    }

    @Override
    public void updateStatistics4NHICTest(UserDataset ud) {
        String tableName = "nhic2.integrated_2013";
        String sql = "select GROUPING_ID() as group_ID,JK_SEX GENDER, JK_AGE_GROUP AGE_GROUP, JK_SIDO SIDO,year(t30_recu_fr_dt) year, month(t30_recu_fr_dt) month, count(T20_KEY_SEQ) n_statement, count(distinct T20_PERSON_ID) as n_patient, sum(T20_DMD_TRAMT) as n_expense "
                + "from " + tableName + " "
                + "GROUP BY year(t30_recu_fr_dt), month(t30_recu_fr_dt), JK_SEX, JK_AGE_GROUP, JK_SIDO WITH CUBE";
        List<Map<String, Object>> rows1 = hiveJdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows1) {
            System.out.println(String.valueOf(row.toString()));
            String.valueOf(row.get("group_id"));
        }

    }

    @Override
    public void testCreateTable() {
        String sql = "CREATE TABLE user_datasets.ud_1_27 AS SELECT * FROM hira_r2.nps_integrated_2013 WHERE  t20_year IN (2013) limit 10";
        hiveJdbcTemplate.execute(sql);
    }

    @Override
    public void createTable(String tableName, String query) throws DataAccessException {
        // String tableName =
        // "user_datasets.ud_"+dataDef.getMembertouserdataset().getMem_idx()+"_"dataDef.getUserdataset().getUd_idx();
        String sql = "CREATE TABLE " + tableName + " AS " + query;
        hiveJdbcTemplate.execute(sql);

    }

    @Override
    public void testTableSize() {
//		show tblproperties user_datasets.ud_1_89;
        @SuppressWarnings("unused")
        String tableName = "nhic2.integrated_2013";
        String sql = "show tblproperties user_datasets.ud_1_89";
        List<Map<String, Object>> rows1 = hiveJdbcTemplate.queryForList(sql);
//		System.out.println(rows1);
//		for (Map<String, Object> row : rows1) {
//			Set<String> keySet = row.keySet();
//			for(String key: keySet){
//				String value = String.valueOf(row.get(key));
//				System.out.println(key+"::"+value);
//				
//			}
//		}
        Map<String, Object> m = rows1.get(3);
        System.out.println(m);
        @SuppressWarnings("unused")
        long size = Long.parseLong(String.valueOf(m.get("value")));
    }

    @Override
    public void updateTableSize(DataDefinition dataDef) {
        int user_idx = dataDef.getMembertouserdataset().getMem_idx();
        int user_dataset_idx = dataDef.getUserdataset().getUd_idx();
        String userTableName = "user_datasets.ud_" + user_idx + "_" + user_dataset_idx;
        String sql = "show tblproperties " + userTableName;//"user_datasets.ud_1_89";
        List<Map<String, Object>> rows1 = hiveJdbcTemplate.queryForList(sql);
        Map<String, Object> m = rows1.get(3);
        long size = Long.parseLong(String.valueOf(m.get("value")));
        sqlServerJdbcTemplate.update("UPDATE _user_dataset SET ud_result_file_size=? WHERE ud_idx = ?", new Object[]{size, user_dataset_idx});
    }

}
