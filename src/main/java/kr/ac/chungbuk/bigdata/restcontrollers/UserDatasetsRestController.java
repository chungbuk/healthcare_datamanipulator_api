package kr.ac.chungbuk.bigdata.restcontrollers;

import kr.ac.chungbuk.bigdata.models.DataDefinition;
import kr.ac.chungbuk.bigdata.models.Member;
import kr.ac.chungbuk.bigdata.utilities.Response;
import kr.ac.chungbuk.bigdata.utilities.ResponseCode;
import kr.ac.chungbuk.bigdata.repositories.HealthCareDAO;
import kr.ac.chungbuk.bigdata.services.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author charisma0629
 */
@RestController
@RequestMapping(value="/api/data")
public class UserDatasetsRestController {

    @Autowired
    private HealthCareDAO healthCareDAO;

    @Autowired
    private DataService dataService;

    @RequestMapping(value = "/put", method = RequestMethod.POST)
    public Response<DataDefinition> putData(@RequestBody DataDefinition dataDefinition){
        try{
            if(dataService.f(dataDefinition)){
                return new Response<DataDefinition>(dataDefinition, ResponseCode.SUCCESS);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return new Response<DataDefinition>(dataDefinition, ResponseCode.FAILURE);
    }

    @RequestMapping(value = "/members", method = RequestMethod.POST)
    public Response<List<Member>> getAllMembers(@RequestBody DataDefinition dataDefinition){
        List<Member> members = new ArrayList<Member>();
        try{
            members = healthCareDAO.findAllMember();
            System.out.println(members);
            return new Response<>(members, ResponseCode.SUCCESS);
        }catch(Exception ex){
            ex.printStackTrace();
            return new Response<>(members, ResponseCode.FAILURE);
        }
    }



}
