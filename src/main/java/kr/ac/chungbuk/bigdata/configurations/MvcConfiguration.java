package kr.ac.chungbuk.bigdata.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.sql.DataSource;

/**
 * Created by PENHCHET on 11/21/2016.
 */
@Configuration
public class MvcConfiguration extends WebMvcConfigurerAdapter{

    @Bean(name = "sqlServerDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource sqlServerDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "thriftServerDataSource")
    @ConfigurationProperties(prefix = "spring.thrift-datasource")
    public DataSource thriftServerDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Autowired
    @Qualifier("sqlServerDataSource")
    private DataSource dataSource;

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name= "sqlServerJdbcTemplate")
    public JdbcTemplate sqlServerJdbcTemplate(){
        return new JdbcTemplate(sqlServerDataSource());
    }

    @Bean(name= "thriftServerJdbcTemplate")
    public JdbcTemplate thriftServerJdbcTemplate() {
        return new JdbcTemplate(thriftServerDataSource());
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
        registry.addResourceHandler("/download/csv/**").addResourceLocations("file:/home/hadoop/");
    }
}
