package kr.ac.chungbuk.bigdata.services;

import kr.ac.chungbuk.bigdata.models.DataDefinition;

import java.sql.SQLException;

/**
 * Created by PENHCHET on 11/22/2016.
 */
public interface DataService {

    public boolean f(DataDefinition dataDefinition) throws SQLException;

}
