package kr.ac.chungbuk.bigdata.services;

import kr.ac.chungbuk.bigdata.models.Code;
import kr.ac.chungbuk.bigdata.models.DataDefinition;
import kr.ac.chungbuk.bigdata.utilities.ExecuteLogHandler;
import kr.ac.chungbuk.bigdata.repositories.HealthCareDAO;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by PENHCHET on 11/22/2016.
 */

@Service
public class DataServiceImpl implements DataService{

    private Logger logger = Logger.getLogger(DataServiceImpl.class);

    @Autowired
    private HealthCareDAO healthCareDAO;

    @Override
    public boolean f(DataDefinition dataDef) throws SQLException {

        boolean isTheEndOfTask = true;

        /* CHK the record exists */
        chkRecords(dataDef);
		/* UPDATE the record started */
        updateRecordsStart(dataDef);
        try {
            callExternalProcess(dataDef);
        } catch (Exception e) {
            updateRecordsError(dataDef);
        }
		/* CALL EXTERNAL PROCESS */

		/* UPDATE DATABASE RECORD */
        updateRecordsSuccess(dataDef);

		/* UPDATE STATISTICS OF USERDATASET */
        healthCareDAO.updateStatistics(dataDef);
		/* UPDATE TABLE INFORMATION OF USERDATASET */
        healthCareDAO.updateTableInformation(dataDef);
		/* UPDATE TABLE INFORMATION OF USERDATASET */
        healthCareDAO.updateTableSize(dataDef);
		/* SEND EMAIL */
        sendEmail(dataDef);

        return isTheEndOfTask;
    }

    private void updateRecords(DataDefinition dataDef, String status) {
        healthCareDAO.updateUserDataset(dataDef.getUserdataset(),
                dataDef.getMembertouserdataset(), status);

    }

    private void sendEmail(DataDefinition dataDef) {
        // TODO Auto-generated method stub

    }

    private void updateRecordsStart(DataDefinition dataDef) {
        updateRecords(dataDef, Code.STARTED);
    }

    private void updateRecordsError(DataDefinition dataDef) {
        updateRecords(dataDef, Code.FAILED);
    }

    private void updateRecordsSuccess(DataDefinition dataDef) {
        updateRecords(dataDef, Code.COMPLETED);
    }

    private void chkRecords(DataDefinition dataDef) throws NullPointerException {
        boolean exist = healthCareDAO.checkUserDataset(
                dataDef.getUserdataset(), dataDef.getMembertouserdataset());
        if (!exist) {
            logger.error("UserDataset is NOT VALID:" + dataDef.toString());
            throw new NullPointerException("UserDataset is NOT VALID");
        }

    }

    private int callExternalProcess(DataDefinition dataDef) throws IOException,
            DataAccessException {
        // ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        // PumpStreamHandler streamHandler = new PumpStreamHandler(new
        // ExecuteLogHandler(logger, Level.DEBUG),
        // new ExecuteLogHandler(logger, Level.ERROR));
        PumpStreamHandler streamHandler = new PumpStreamHandler();
        // PumpStreamHandler pumpStreamHandler = new PumpStreamHandler(new
        // ExecuteLogHandler(logger, Level.DEBUG), new ExecuteLogHandler(logger,
        // Level.ERROR));

        DefaultExecutor executor = new DefaultExecutor();
        executor.setExitValue(1);
        executor.setStreamHandler(streamHandler);

		/* CHK DIR AND IF NOT EXISTS MKDIR OUTPUTDIR */
        String HADOOP_PATH = "/home/hadoop/hadoop/bin/hadoop";
        String nameNodeURI = "hdfs://namenode:8020";
        int userName = dataDef.getMembertouserdataset().getMem_idx();
        int idx = dataDef.getUserdataset().getUd_idx();
        String userDirectoryPathPrefix = nameNodeURI + "/user_dataset/"
                + userName + "/" + idx;
        String mkdirCmd = "dfs -mkdir -p " + userDirectoryPathPrefix;
        CommandLine cmdl = new CommandLine(HADOOP_PATH);
        cmdl.addArgument(mkdirCmd);
        int exitValue = executor.execute(cmdl);
        logger.debug("CHK DIR :" + exitValue);

        String userTableName = "user_datasets.ud_" + userName + "_" + idx;

        String dbName = dataDef.getUserdataset().getUd_terms_db_op1().trim();
        // String others = dataDef.getDatabase()[0].get;//NIS, NPS, APS, PPS etc
        String year = dataDef.getUserdataset().getUd_terms_db_op2();

        String disease = dataDef.getUserdataset().getUd_terms_disease_code()
                .replaceAll("\\.", "").replaceAll(",", "','");
        // String revisedString = original.replaceAll("\\.", "").replaceAll(",",
        // "','");
        // System.out.println("'"+revisedString+"'");
        String medicine = dataDef.getUserdataset().getUd_terms_medicine_code()
                .replaceAll("\\.", "").replaceAll(",", "','");
        String t30_div_cd = dataDef.getUserdataset()
                .getUd_terms_materials_code().replaceAll("\\.", "")
                .replaceAll(",", "','");

        String treatment_t30_div_cd = dataDef.getUserdataset()
                .getUd_terms_treatment_code().replaceAll("\\.", "")
                .replaceAll(",", "','");
        StringBuilder sb = new StringBuilder();
        String query = "";
        String queryPrefix = "SELECT * FROM ";
        logger.debug("YEAR::" + year);
        logger.debug("disease::" + disease);
        logger.debug("medicine::" + medicine);
        logger.debug("t30_div_cd::" + t30_div_cd);
        logger.debug("treatment_t30_div_cd::" + treatment_t30_div_cd);

        switch (dbName) {
            case Code.HIRA:
                sb.append(queryPrefix);
                sb.append("hira_r2.nps_integrated_2013 WHERE ");
                if (!year.isEmpty())
                    sb.append(" t20_year IN (" + year + ") AND");
                if (!disease.isEmpty()) {
                    disease = "'" + disease + "'";
                    sb.append(" t40_DMD_sick_sym IN (" + disease + ") AND");
                }
                if (!medicine.isEmpty()) {
                    medicine = "'" + medicine + "'";
                    sb.append(" T53_GNL_NM_CD_8 IN (" + medicine + ") AND");
                }
                if (!t30_div_cd.isEmpty()) {
                    t30_div_cd = "'" + t30_div_cd + "'";
                    sb.append(" t30_div_cd IN (" + t30_div_cd + ") OR");
                }
                if (!treatment_t30_div_cd.isEmpty()) {
                    treatment_t30_div_cd = "'" + treatment_t30_div_cd + "'";
                    sb.append(" t30_div_cd IN (" + treatment_t30_div_cd + ")");
                }
                String queryString = sb.toString();
                String ss = queryString.substring(queryString.length() - 3,
                        queryString.length());

                if (ss.contains("OR")) {
                    query = queryString.substring(0, queryString.length() - 2);
                    System.out.println(query);
                    System.out.println("SQL OR::" + query);
                } else if (ss.contains("AND")) {
                    query = queryString.substring(0, queryString.length() - 3);
                    System.out.println("SQL AND::" + query);
                } else {
                    query = queryString;
                }
                break;
            case Code.NHIC:

                sb.append(queryPrefix);
                sb.append("nhic2.integrated_2013 WHERE ");
                if (!year.isEmpty())
                    sb.append(" jk_stnd_y IN (" + year + ") AND");
                if (!disease.isEmpty()){
                    disease = "'" + disease + "'";
                    sb.append(" t40_sick_sym IN (" + disease + ") AND");
                }
                if (!medicine.isEmpty()){
                    medicine = "'" + medicine + "'";
                    sb.append(" T60_GNL_NM_CD IN (" + medicine + ") AND");
                }
                if (!t30_div_cd.isEmpty()){
                    t30_div_cd = "'" + t30_div_cd + "'";
                    sb.append(" t30_div_cd_8 IN (" + t30_div_cd + ") OR");
                }
                if (!treatment_t30_div_cd.isEmpty()){
                    treatment_t30_div_cd = "'" + treatment_t30_div_cd + "'";
                    sb.append(" t30_div_cd_8 IN (" + treatment_t30_div_cd + ")");
                }
                String queryString2 = sb.toString();
                String ss2 = queryString2.substring(queryString2.length() - 3,
                        queryString2.length());

                if (ss2.contains("OR")) {
                    query = queryString2.substring(0, queryString2.length() - 2);
                } else if (ss2.contains("AND")) {
                    query = queryString2.substring(0, queryString2.length() - 3);
                } else {
                    query = queryString2;
                }
                break;
            default:
                break;
        }
		/* ONLY FOR TEST */
        // query = query + " limit 10";
		/* ONLY FOR TEST */
        logger.debug("ONLY FOR TEST SQL::" + query);

		/* SPARK */
        // String spark = "/home/hadoop/spark/bin/spark-sql";
        // String sparkCmd =
        // " --master spark://192.168.0.122:7077 -e 'CREATE TABLE " +
        // userTableName + " AS " + query + "'";
        // String command = spark + sparkCmd;
        // logger.debug("SPARK COMMAND ==> " + command);
        //
        // CommandLine commandline = CommandLine.parse(command);
        // exitValue = executor.execute(commandline);
        //
        // logger.debug(" EXIT_CODE :" + exitValue + ", command =" + command);
		/* SPARK */
		/* CREATE USER DATASET TABLE */
        healthCareDAO.createTable(userTableName, query);

        try{
            //TODO: To start Exporting Data to CSV File
            exitValue = startExportToCsv("SELECT * FROM " + userTableName, "/home/hadoop/user_datasets/ud_" + userName + "_" + idx);

            //TODO: TO get the filename
            String LINUX_PATH = "/home/hadoop/user_datasets/";
            File[] files = new File(LINUX_PATH + "ud_" + userName + "_" + idx).listFiles();
            String fileName = "";
            for(File file: files){
                if(file.getName().startsWith("part-")){
                    fileName = file.getName();
                    break;
                }
            }

            //TODO: To update the download link the SqlServer
            String downloadURL = "http://1.246.219.212:18000/HEALTHCARE/download/csv/user_datasets/ud_" + userName + "_" + idx + "/" + fileName;
            dataDef.setDownloadURL(downloadURL);
            System.out.println("DOWNLOAD URL ===> " + downloadURL);

        }catch(Exception ex){
            ex.printStackTrace();
        }

        return exitValue;

    }

    //TODO: To Start Exporting Data To CSV File
    private int startExportToCsv(String sql, String saveLocation){

        //TODO: To create the Spark Configuration
        SparkConf sparkConf = new SparkConf(true).setAppName("SparkSQL CSV Integration").setMaster("spark://192.168.0.122:7077");
        sparkConf.set("javax.jdo.option.ConnectionURL", "jdbc:mysql://was.bigdatacenter.org:3306/hivedb?createDatabaseIfNotExist=true");
        sparkConf.set("javax.jdo.option.ConnectionPassword","Dbnis3258!@#$");
        sparkConf.set("javax.jdo.option.ConnectionUserName","hiveuser");
        sparkConf.set("javax.jdo.option.ConnectioNDriverName", "com.mysql.jdbc.Driver");
        sparkConf.set("hive.metastore.warehouse.dir", "hdfs://192.168.0.122:8020/user/hive/warehouse");
        sparkConf.set("spark.jars.package", "com.databricks.spark-csv_2.10:1.4.0");
        sparkConf.set("spark.executor.memory", "16g");

        //TODO: To create the Spark Session
        SparkSession sparkSession = SparkSession
                .builder()
                .config(sparkConf)
                .enableHiveSupport()
                .getOrCreate();

        Dataset<Row> dataFrame = sparkSession.sql(sql);

        String HADOOP_PATH = "/home/hadoop/hadoop/bin/hadoop";
        String NAMENODE_URI = "hdfs://namenode:8020";
        String LINUX_PATH = "/home/hadoop/user_datasets/";

        int exitValue = 0;

        try{
            //TODO: To save the dataFrame to the csv file location in the hadoop directory with column header
            dataFrame.repartition(1)
                    .write()
                    .mode(SaveMode.Overwrite)
                    .format("com.databricks.spark.csv")
                    .option("header", "true")
                    .option("codec", "org.apache.hadoop.io.compress.GzipCodec")
                    .save(NAMENODE_URI + saveLocation);

            //TODO: To start to copy csv File from Hadoop to Local Server
            PumpStreamHandler pumpStreamHandler = new PumpStreamHandler(new ExecuteLogHandler(logger, Level.DEBUG), new ExecuteLogHandler(logger, Level.ERROR));
            DefaultExecutor executor = new DefaultExecutor();
            executor.setStreamHandler(pumpStreamHandler);

            String command = HADOOP_PATH + " dfs -copyToLocal "+ saveLocation + " " + LINUX_PATH;
            System.out.println("COMMAND ==> " + command);

            CommandLine commandLine = CommandLine.parse(command);
            exitValue = executor.execute(commandLine);

        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            sparkSession.stop();
        }

        return exitValue;
    }
}
